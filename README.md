# IDP-Z3-OEL

IDP-Z3-OEL is an extension of [IDP-Z3 system](https://www.idp-z3.be/)
supporting [Ordered Epistemic Logic](https://dblp.org/rec/conf/kr/VlaeminckVBD12.html).

## Setup the project

Note: All commands are executed from the project root directory!

First, clone the source code of this repository and navigate with your command line to the root folder.

1) **Install git submodules**, for the first time use the command:

```
git submodule update --init --recursive
```

To update modules use: 
```
git submodule update --recursive --remote
```

2) Create & activate a **virtual environment**, and **install the requirements**.
```
python3 -m venv ./venv
source venv/bin/activate
pip install -r requirements.txt
```

The OEL and eDMN tools are ready to use!


## How to use the tool

To run an OEL model-expand for a theory in an input file, execute the following:
```
python -m oel model-expand <file_path> <theory>
```

Example files are under the directory `./examples/`. So, for example: 
```
python -m oel model-expand ./examples/test-1.idp T1
```

This repository also includes an editor.
To run it, execute the following:
```
python -m oel editor
```

To deactivate the virtual environment, run the command:
```
deactivate
```

## How to use the editor

To use the editor simply load an `idpz3` file specify the name of the theory to be used and run model-expand. 
Additionally, the editor supports the conversion of eDMN files, simply load an `xlsx` file satisfying eDMN constraints and it will be automatically translated into OEL theory. 
For the library of examples, please explore the `./examples/` directory.


## Build the editor

You can also generate a binary for the editor, based on pyinstaller.
First, make sure you have activated the virtual environment and installed the requirements.
Next, install pyinstaller:
```
pip install pyinstaller
```

To build the binary, run the following command:
```
pyinstaller --add-binary ./venv/lib/python3.11/site-packages/z3/lib:. --add-data ./venv/lib/python3.11/site-packages/idp_engine/:idp_engine --add-data ./examples/:examples idp-z3-oel-editor.py
```

An executable file will be generated in the `dist` directory.
Note that this only works if your virtual environment is named `venv`. The command from above should be updated if another name is used.

## Useful links

- [IDP-Z3 python api](http://docs.idp-z3.be/en/latest/IDP-Z3.html#python-api)
- [Order Epistemic Logic](https://dblp.org/rec/conf/kr/VlaeminckVBD12.html)






