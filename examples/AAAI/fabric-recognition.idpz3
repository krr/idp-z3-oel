vocabulary V {
    type Message := {askOverallTexture, askFloats, askFloatLength} 
    questions : (Message) -> Bool
    
    plainWeave : () -> Bool
    alternatingWarp : () -> Bool
    twillWeave : () -> Bool
    diagonalTexture : () -> Bool
    hasFloats : () -> Bool
    warpOffsetEQ1 : () -> Bool
    satinWeave : () -> Bool
    warpOffsetGTl : () -> Bool
    solidPlain : () -> Bool
    oneColor : () -> Bool
    percale : () -> Bool
    cotton : () -> Bool
    balanced : () -> Bool
    smooth : () -> Bool
    organdy : () -> Bool
    sheer : () -> Bool
    organza : () -> Bool
    silk : () -> Bool
    patternPlain : () -> Bool
    colorGroups : () -> Bool
    plaid : () -> Bool
    warpStripe : () -> Bool
    fillStripe : () -> Bool
    gingham : () -> Bool
    equalStripe : () -> Bool
    basketWeave : () -> Bool
    groupedWarps : () -> Bool
    oxford : () -> Bool
    type2Tol : () -> Bool
    monksCloth : () -> Bool
    type2To2 : () -> Bool
    type4To4 : () -> Bool
    hopsacking : () -> Bool
    rough : () -> Bool
    open : () -> Bool
    ribbedWeave : () -> Bool
    someThicker : () -> Bool
    crossRibbed : () -> Bool
    thickerFill : () -> Bool
    faille : () -> Bool
    smallRib : () -> Bool
    flatRib : () -> Bool
    grosgrain : () -> Bool
    roundRib : () -> Bool
    bengaline : () -> Bool
    mediumRib : () -> Bool
    ottoman : () -> Bool
    heavyRib : () -> Bool
    flannel : () -> Bool
    napped : () -> Bool
    wool : () -> Bool
    lenoWeave : () -> Bool
    crossedWarps : () -> Bool
    marquisette : () -> Bool
    fillPile : () -> Bool
    extraFill : () -> Bool
    warpPile : () -> Bool
    extraWarp : () -> Bool
    velvet : () -> Bool
    cut : () -> Bool
    terry : () -> Bool
    uncut : () -> Bool
    reversible : () -> Bool
    corduroy : () -> Bool
    alignedPile : () -> Bool
    velveteen : () -> Bool
    staggeredPile : () -> Bool
    evenTwill : () -> Bool
    floatEQSink : () -> Bool
    fillingFaced : () -> Bool
    floatLTSink : () -> Bool
    warpFaced : () -> Bool
    floatGTSink : () -> Bool
    denim : () -> Bool
    coloredWarp : () -> Bool
    whiteFill : () -> Bool
    drill : () -> Bool
    serge : () -> Bool
    satin : () -> Bool
    warpFloats : () -> Bool
    sateen : () -> Bool
    fillFloats : () -> Bool
    moleskin : () -> Bool
    sameThickness : () -> Bool



    floatLength  : () -> Bool
    weave : () -> Bool
    overallTexture : () -> Bool
    floats : () -> Bool
}


theory Tf:V {
    {
        plainWeave() <- alternatingWarp().
        twillWeave() <- diagonalTexture().
        twillWeave() <- hasFloats() & warpOffsetEQ1(). 
        satinWeave() <- hasFloats() & warpOffsetGTl().

        solidPlain() <- plainWeave() & oneColor().
        percale() <- solidPlain() & cotton() & balanced() & smooth(). 
        organdy() <- solidPlain() & cotton() & sheer().
        organza() <- solidPlain() & silk() & sheer().

        patternPlain() <- plainWeave() & colorGroups().
        plaid() <- patternPlain() & warpStripe() & fillStripe(). 
        gingham() <- plaid() & equalStripe().

        basketWeave() <- plainWeave() & groupedWarps().
        oxford() <- basketWeave() & type2Tol().
        oxford() <- basketWeave() & thickerFill().
        monksCloth() <- basketWeave() & type2To2() & sameThickness().
        monksCloth() <- basketWeave() & type4To4() & sameThickness().
        hopsacking() <- basketWeave() & rough() & open().

        ribbedWeave() <- plainWeave() & someThicker().
        crossRibbed() <- ribbedWeave() & thickerFill().
        faille() <- crossRibbed() & smallRib() & flatRib().
        grosgrain() <- crossRibbed() & smallRib() & roundRib().
        bengaline() <- crossRibbed() & mediumRib() & roundRib().
        ottoman() <- crossRibbed() & heavyRib() & roundRib().

        flannel() <- plainWeave() & cotton() & napped().
        flannel() <- twillWeave() & cotton() & napped().
        flannel() <- plainWeave() & wool() & napped().
        flannel() <- twillWeave() & wool() & napped().

        lenoWeave() <- plainWeave() & crossedWarps().
        marquisette() <- lenoWeave() & open().

        fillPile() <- plainWeave() & extraFill().
        warpPile() <- plainWeave() & extraWarp().
        velvet() <- warpPile() & cut().
        terry() <- fillPile() & uncut().
        terry() <- fillPile() & reversible().
        corduroy() <- fillPile() & cut() & alignedPile().
        velveteen() <- fillPile() & cut() & staggeredPile().

        evenTwill() <- twillWeave() & floatEQSink().
        fillingFaced() <- twillWeave() & floatLTSink().
        warpFaced() <- twillWeave() & floatGTSink().
        denim() <- warpFaced() & coloredWarp() & whiteFill().
        drill() <- warpFaced() & oneColor().
        serge() <- evenTwill() & heavyRib().

        satin() <- satinWeave() & warpFloats() & smooth().
        sateen() <- satinWeave() & fillFloats() & smooth().
        moleskin() <- satinWeave() & cotton() & napped().
    }
    //diagonalTexture().
    //floatGTSink().
    //coloredWarp().
    //whiteFill().

    //~denim().

}

theory Udb:V {
    {
        floatLength()  <- floatLTSink() | floatEQSink() | floatGTSink().
        weave() <- warpOffsetEQ1() | warpOffsetEQ1() | warpOffsetGTl().
        overallTexture() <- plainWeave() | twillWeave() | satinWeave().
        floats() <- hasFloats().
    }

    //warpOffsetEQ1().
    //floatGTSink().
    twillWeave().

}


theory Ask:V {
    {
        questions(askOverallTexture) <- ~K[Udb][weave()] & ~K[Udb][overallTexture()].
        questions(askFloats) <- ~K[Udb][weave()] & ~K[Udb][floats()].
        questions(askFloatLength) <- ~K[Udb][floatLength()] & K[Udb][twillWeave()].
    }
}