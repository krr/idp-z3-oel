import sys
import os

# Add the directory to sys.path
module_path = os.path.abspath(os.path.join('..', '..'))
if module_path not in sys.path:
    sys.path.append(module_path)

# Add the directory to sys.path
module_path = os.path.abspath(os.path.join('..', '..', 'oel','edmn'))
if module_path not in sys.path:
    sys.path.append(module_path)
    

# Now you can import the module
import oel
import edmn

from oel import cli, __app_name__

from edmn import edmn

def main():
    print("Trasnlating eDMN to OEL...")
    edmn.to_OEL("greeting-ext.xlsx", "greeting-ext.idpz3")
    print("Translation saved in 'greeting-ext.idpz3'.")

    print("\n")
    print("Runing the decision making process...")
    cli.model_expand("greeting-ext.idpz3", "DEC")
    print("Decision making finished!")

if __name__ == "__main__":
    main()