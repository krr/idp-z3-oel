# "Greeting a Customer with Unknown Data" challenge

This file contains instructions on how to use the proposed solution for the "Greeting a Customer with Unknown Data" challenge that was published in 2016 as part of [Decision Management Community](https://dmcommunity.org/challenge/challenge-aug-2016/).

## eDMN solution

The file containing eDMN tables is `greeting-ext.xlsx`. 
To make changes open this file and modify the tables.

## Making decision 

**Prerequisite:** After downloading this repository, make sure to first follow the instructions from the main [README.md](https://gitlab.com/krr/idp-z3-oel/-/blob/main/README.md?ref_type=heads) file (in the root of the repository) under the title "Setup the project". Make the software ready to use. Once that is done get back to this file. 


To execute the eDMN tables and make decisions run the Python script `run.py` (from the same directory):

```python run.py```

**Note:** After running this command the solver will provide multiple models (at most 10), these models will have the same assignments for the decision variables, the only difference is in the possible values for input variables.

## How it works

The underlying process: eDMN tables are first translated to OLE theory (file `greeting-ext.idpz3`), then the solver for OEL is called.


