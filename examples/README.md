# Examples

This directory contains a collection of examples of Order Epistemic Logic (OEL).
Follwoing is the short description and source information of the exmaples:

## A simple OEL example (test-1-simple)

This example is a trivial one and serves for testing and demonstrating the system.

## A simple example with variables (test-2-variable)

This example is a trivial one including variables and serves for testing and demonstrating the system.

## An interview example (test-3-interview-1, test-3-interview-2)

This example is taken from the (paper)[https://dblp.org/rec/conf/kr/VlaeminckVBD12.html] where it is formalized using OEL.
The example originally appears int the (paper)[https://dblp.org/rec/journals/ngc/GelfondL91.html]. 

## Default logic (test-4-default-logic)

The example is very old and can be sead that it is folck, the exact example is taken from (Wikipedia article)[https://en.wikipedia.org/wiki/Default_logic]. 

## Greeting example (test-5-greeting-1, test-5-greeting-objective-vs-epistemic)

The greeting example originates from the (DMN challenge examples)[https://dmcommunity.org/challenge/challenge-aug-2016/].
It is further worked out and discussed in (this paper)[https://dblp.org/rec/conf/ruleml/MarkovicVVD22.html].

## Prolog fabric example (test-6-fabric-example)

This example is from the book ( Computing with Logic; Logic Programming with Prolog, David Maier & David S. Warren)[https://philpapers.org/rec/MAICWL].