from idp_engine import IDP, Theory
from idp_engine.Expression import TRUE, FALSE
import re

letters = ['N', 'M', 'K']
more_letters = ['A', 'B', 'C']

th2_voc = """
q, z: -> Bool
"""
th2_theory = """
K_T1[q()] => z().
"""

th1_voc = """
p, q: -> Bool
"""
th1_theory = """
p() => q().
p().
"""

# Given theory 2, find if any K operators are used on the level below it.
OEL = re.findall(r'K_T1\[(.*?)\]', th2_theory)

for i, phi in enumerate(OEL):
    # We need to introduce a new symbol with the same arity in the vocabulary.
    letter = letters[i]
    th1_voc += f"{letter}: -> Bool\n"

    # We need to define this new symbol.
    th1_theory += f"{{ {letter}() <- {phi}.}}"


# Execute propagation on theory 1
th1 = f'vocabulary {{ {th1_voc} }} \n theory {{ {th1_theory} }}'
kb = IDP.from_str(th1)

T, S = kb.get_blocks("T, S")
theory = Theory(T)

# prop_theory.assignments now contains the propagated values.
prop_theory = theory.propagate()


th2_struct = ""
for i, phi in enumerate(OEL):
    # Replace the K operator by a new symbol.
    other_letter = more_letters[i]
    th2_voc += f"{other_letter}: -> Bool"
    th2_theory = re.sub(r'K_T1\[.*?\]', f'{other_letter}()', th2_theory)

    # Now we need to update our structure accordingly.
    # First, check if phi could be propagated.
    letter = letters[i]
    assig = prop_theory.assignments[f'{letter}()']

    # If a value for phi was propagated, it is known.
    if assig.value.same_as(TRUE) or assig.value.same_as(FALSE):
        th2_struct += f"\n{other_letter} := true.\n"
    else:
        th2_struct += f"\n{other_letter} := false.\n"

th2 = f'vocabulary {{ {th2_voc} }} \n theory {{ {th2_theory} }} \n structure {{ {th2_struct} }}'

# Finally, execute model expansion on theory 2
kb = IDP.from_str(th2)
T, S = kb.get_blocks("T, S")
theory = Theory(T, S)
for model in theory.expand():
    print(model)

print(th1)
print(th2)
