import os
import tkinter as tk
from tkinter import filedialog
import sys
from oel.engine import Engine

# Because our eDMN files are one level lower, we explicitly add them to the
# PATH.
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path + '/edmn')
from edmn.edmn import to_OEL


class Editor:
    # Constants
    initial_example = "/examples/test-1-simple.idpz3"
    initial_theory = "T2"
    gui = NotImplemented
    file_field = NotImplemented
    save_button = NotImplemented
    button_explore = NotImplemented
    code_editor = NotImplemented
    theory_field = NotImplemented
    mx_button = NotImplemented
    resolve_button = NotImplemented
    output_field = NotImplemented

    def init(self):
        self.gui = tk.Tk()
        self.gui.title("IDP-Z3 OEL (testing editor)")
        self.gui.geometry("900x900")

        # Create file group
        controle_frame_file = tk.LabelFrame(self.gui, text="Files")

        # Loaded file path
        label_file_explorer = tk.Label(controle_frame_file, text = "File path:", width = 10)
        label_file_explorer.pack(side=tk.LEFT)

        # Path entry
        self.file_field = tk.Entry(controle_frame_file, width=70)
        self.file_field.pack(side=tk.LEFT)

        # Create the save button
        self.save_button = tk.Button(controle_frame_file, text="Save", command = self.save_file)
        self.save_button.pack(side=tk.RIGHT, padx = 10)

        # Create the browse button
        self.button_explore = tk.Button(controle_frame_file, text = "Browse Files", command = self.browseFiles)
        self.button_explore.pack(side=tk.RIGHT)

        # Pack file group
        controle_frame_file.pack(fill=tk.X)

        # Create the code editor
        self.code_editor = tk.Text(self.gui, height=35, width=100)
        self.code_editor.pack(fill=tk.BOTH, expand=True)

        # Create controle group
        controle_frame = tk.LabelFrame(self.gui, text="Commands", pady=5)

        # Theory entry label
        theory_lable = tk.Label(controle_frame, text="Theory name:", width=20)
        theory_lable.pack(side=tk.LEFT)

        # Theory entry
        self.theory_field = tk.Entry(controle_frame, width=5)
        self.theory_field.pack(side=tk.LEFT)

        # Create the run button
        self.mx_button = tk.Button(controle_frame, text="Model expand", command = self.model_expand)
        self.mx_button.pack(side=tk.RIGHT)

        # Create the run button
        self.resolve_button = tk.Button(controle_frame, text="Resolved theory", command = self.resolve)
        self.resolve_button.pack(side=tk.RIGHT, padx = 10)

        # Pack controle
        controle_frame.pack(fill=tk.X)

        # Create the output field
        self.output_field = tk.Text(self.gui, height=16, width=100)
        self.output_field.pack(fill=tk.X)

    def init_fileds(self):
        exm = os.getcwd() + self.initial_example

        try:
            with open(str(exm)) as f:
                code = f.read()
                self.input_field_insert(code)
                self.path_field_insert(exm)
                self.theory_field_insert(self.initial_theory)
        # In case of a faliour
        except OSError:
            self.output_field_insert("Error reading the file!!!")


    def browseFiles(self):
        try:
            cwd = os.getcwd() + "/examples/"
            filename = filedialog.askopenfilename(initialdir=cwd,
                                                  title="Select a File",
                                                  filetypes=(("IDP files",
                                                              "*.idp*"),
                                                             ("eDMN",
                                                              "*.xlsx"),
                                                             ("all files",
                                                              "*.*")))
            if filename:
                # Update the current file path
                self.path_field_insert(str(filename))

                code = ""
                # Try to open and read the file
                try:
                    if filename.endswith('.xlsx'):
                        code = to_OEL(filename)
                    else:
                        with open(str(filename)) as f:
                            code = f.read()
                # In case of failure
                except OSError:
                    self.output_field_insert("Error reading the file.")

                self.input_field_insert(code)
        except:
            self.output_field_insert("File browse aborted!")

    def save_file(self):
        curent_file_path = self.file_field.get()
        if curent_file_path:
            try:
                with open(curent_file_path, 'w') as f:
                    f.write(self.code_editor.get("1.0", tk.END))
                self.output_field_insert("File saved!") 
            except OSError:
                code = "Error reading the file!!!"
        else:
            self.output_field_insert("No file path!") 

    def model_expand(self):
        theory_name = self.theory_field.get()
        if not theory_name:
            self.output_field_insert("You have to enter some theory name!")
        else:
            try:
                eng = Engine()
                eng.load_from_string(self.code_editor.get("1.0", tk.END))
                self.output_field_insert(eng.model_expand(theory_name))
            except Exception as e:
                self.output_field_insert(f'Model expand failed with: "{e}"')

    def resolve(self):
        theory_name = self.theory_field.get()
        if not theory_name:
            self.output_field_insert("You have to enter some theory name!")
        else:
            try:
                eng = Engine()
                eng.load_from_string(self.code_editor.get("1.0", tk.END))
                self.output_field_insert(eng.print_resolved_theory(theory_name))
            except Exception as e:
                self.output_field_insert(f'Model expand failed with: "{e}"')

    def path_field_insert(self, text):
        self.file_field.delete(0,tk.END)
        self.file_field.insert(0,text)
    
    def theory_field_insert(self, text):
        self.theory_field.delete(0,tk.END)
        self.theory_field.insert(0,text)

    def input_field_insert(self, text):
        self.code_editor.delete("1.0","end")
        self.code_editor.insert(tk.END, text)

    def output_field_insert(self, text):
        self.output_field.delete("1.0","end")
        self.output_field.insert(tk.END, text)

    def create_window(self):
        # Start the main event loop
        self.gui.mainloop()

    def start_gui(self):
        self.init()
        self.init_fileds()
        self.create_window()
    


    

    
