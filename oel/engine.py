"""This module provides the OEL engine."""
# oel/engine.py

import typer
import random
import string

from pathlib import Path

from oel import SUCCESS, FILE_ERROR
from oel import idp_code_processor as idpcp
from idp_engine import IDP, Theory

class Engine:

    # Constants
    k_prefix = "__K_"

    # Code related variables
    _vocabulary_block = ""
    _theory_blocks = {}
    _code = ""

    def load_from_string(self, code: str) -> int:
        """Read the code from string."""
        
        self._code = code
        self._vocabulary_block, self._theory_blocks = idpcp.get_voc_and_theories(self._code)

    def load_from_file(self, file_path: Path) -> int:
        """Read the code from file."""
        
        # Try to open ande read the file 
        try:
            with open(file_path) as f:
                self._code = f.read()
                self._vocabulary_block, self._theory_blocks = idpcp.get_voc_and_theories(self._code)
        # In case of a faliour
        except OSError:
            raise

    def model_expand(self, theory):
        """Function implementing custom model expand for OEL"""

        # Check that theory for expanding exists
        if theory in self._theory_blocks:
            # Get K atoms from the theory
            k_atoms = idpcp.find_K_atoms(self._theory_blocks[theory])
            
            # Resolve all the K atoms 
            resolved_theory = self.resolve_epistemic_atoms(theory, k_atoms)
            
            # Use standard model expansion now
            the = idpcp.merge(self._vocabulary_block, [resolved_theory], "structure S:V {}")

            # IDP-Z3 api creating theory                    
            kb = IDP.from_str(the)
            T, S = kb.get_blocks(theory + ", S")
            theory = Theory(T, S)

            models_str = ""
            # Print the models of expansion
            for model in theory.expand():
                models_str = models_str + self.filter_model(model) + "\n\n"

            return models_str
        # If there is no theory rais an error
        else:
            raise Exception(f"Theory '{theory}' does not exist!")

    def print_resolved_theory(self, theory):
        """Function resolving OEL theory and printing it"""

        # Check that theory for expanding exists
        if theory in self._theory_blocks:
            # Get K atoms from the theory
            k_atoms = idpcp.find_K_atoms(self._theory_blocks[theory])
            
            # Resolve all the K atoms 
            resolved_theory = self.resolve_epistemic_atoms(theory, k_atoms)
            
            # Use standard model expansion now
            the = idpcp.merge(self._vocabulary_block, [resolved_theory])

            return the
        # If there is no theory rais an error
        else:
            raise Exception(f"Theory '{theory}' does not exist!")

    def filter_model(self, model):
        """Function for removinf K atoms form the model"""
        filtered_lines = [line for line in str(model).splitlines() if not line.startswith('__K_')]
        return '\n'.join(filtered_lines)


    def resolve_epistemic_atoms(self, theory, k_atoms):
        """Function removing K atoms from theories recursevely"""

        # Set of theories used under the K operator
        theories = list(set([pair[0] for pair in k_atoms]))

        # New instance of the theory
        new_theory = self._theory_blocks[theory]

        # Resolve imported theorise
        import_thepries = idpcp.get_import_declarations(new_theory)

        # Per import: 
        for it in import_thepries:
            # Replace the import declaration with the theory
            new_theory = idpcp.import_theory(new_theory, it, self._theory_blocks[it])

        # Per theory eliminate K operator
        for the in theories:
            resolved_the = ""
            the_k_atoms = idpcp.find_K_atoms(self._theory_blocks[the])
            # If theory is epistemic (i.e., there are K atoms): Recursive call to resolve
            if the_k_atoms:
                resolved_the = self.resolve_epistemic_atoms_new(the, the_k_atoms)
            # If theory is objective (i.e., no K atoms): Use the theory, nothing to resolve
            else:
                resolved_the = self._theory_blocks[the]

            # Set of all formulas under K operator for this theory
            phis = [pair[1] for pair in k_atoms if pair[0] == the]
            
            # Per formula do:
            for phi in phis:
                # Get set of free variables in phi
                phi_vars = idpcp.formula_free_vars(self._vocabulary_block, phi)
                
                # If there are free variables, use interpretation
                if phi_vars:
                    # Create a random name
                    new_name = self.k_prefix + the + '_' + ''.join(random.choice(string.ascii_letters) for _ in range(10))
                    # New predicate symbol with signature 
                    new_symb = new_name + ' : ' + ' * '.join(t for v,t in phi_vars) + " -> Bool"
                    # New atomic formula for the new predicate
                    new_atom = new_name + '(' + ','.join(v for v,t in phi_vars) + ')'

                    # Extend vocabulary with new predicate
                    self._vocabulary_block = idpcp.extend_vocabulary(self._vocabulary_block, [new_symb])

                    # Replace K atom in the curent theory 
                    new_theory = new_theory.replace('K['+the+']['+phi+']', new_atom)

                    # Add the equivalence in the sub-theory
                    new_def = "! " + ','.join(v + " in " + t for v,t in phi_vars) + ": " + new_atom + " <=> " + phi + "."
                    resolved_the = idpcp.extend_theory(resolved_the, [new_def])

                    #TODO this propagation could be done once!!! After all formulas are added.
                    # Get propagated values                    
                    new_assig = idpcp.propagate_for_predicate(self._vocabulary_block, resolved_the, new_name)

                    # Add propagations to the current theory
                    new_theory = idpcp.extend_theory(new_theory, [new_assig])
                    
                # If there are not free variables, use entailment
                else:
                    # If the entails phi, replace K phi with true
                    if idpcp.entailment(self._vocabulary_block, resolved_the, phi):
                        new_theory = new_theory.replace('K['+the+']['+phi+']', "true")
                    # Otherwise with false
                    else:
                        new_theory = new_theory.replace('K['+the+']['+phi+']', "false")

        # Return new theory
        return new_theory