"""Top-level package for OEL."""
# oel/__init__.py

__app_name__ = "IDP-Z3(OEL)"
__version__ = "0.1.0"

(
    SUCCESS,
    DIR_ERROR,
    FILE_ERROR,
) = range(3)

ERRORS = {
    DIR_ERROR: "config directory error",
    FILE_ERROR: "reading file error",
}