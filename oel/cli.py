"""This module provides the OEL CLI."""
# oel/cli.py

from typing import Optional
from typing_extensions import Annotated

import typer
import os.path

from oel import ERRORS, __app_name__, __version__
#from oel.editor import Editor
from oel.engine import Engine

app = typer.Typer()
eng = Engine()

# Add the command for executing model expand on a theory
@app.command("model-expand")
def model_expand(file_path: str, theory: str):
    
    if file_path:
        # Read the code from file
        try:
            eng.load_from_file(file_path)
        # Rais an error if there is a problem reading the file
        except Exception as e:
            typer.secho(
                f'Reading file failed with: "{e}"',
                fg=typer.colors.RED,
            )
            raise typer.Exit(1)
    
    # Run the model expand
    try:
        print(eng.model_expand(theory))
    except Exception as e:
        typer.secho(
            f'Model expand failed with: "{e}"',
            fg=typer.colors.RED,
        )

# Add the command for printing resolved version of the theory
@app.command("resolve")
def resolve(file_path: str, theory: str):
    
    if file_path:
        # Read the code from file
        try:
            eng.load_from_file(file_path)
        # Rais an error if there is a problem reading the file
        except Exception as e:
            typer.secho(
                f'Reading file failed with: "{e}"',
                fg=typer.colors.RED,
            )
            raise typer.Exit(1)
    
    # Run the model expand
    try:
        print(eng.print_resolved_theory(theory))
    except Exception as e:
        typer.secho(
            f'Resolve failed with: "{e}"',
            fg=typer.colors.RED,
        )

# Add the command for starting editor
# @app.command("editor")
# def editor():
#     ed = Editor()
#     ed.start_gui()