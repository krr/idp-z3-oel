"""This module provides the IDP code processing utilities."""
# oel/idp_code_processor.py

import sys
import re
import random
import string
import typer

from pathlib import Path
from idp_engine import IDP, Theory, utils
from oel import SUCCESS, DIR_ERROR, FILE_ERROR

def get_voc_and_theories(code):
    """Extract theory and vocabulary blocks form the code string"""

    # Storage variables
    vocabulary_block = ""
    theory_blocks = {}

    current_block = None

    # Split the code into lines
    lines = code.split('\n')
    theory_c = ""

    # Per line
    for line in lines:
        # Skip empty lines and comments
        line = line.strip()
        if not line or line.startswith("//"):
            continue

        # Check if the line starts a new block
        if line.startswith("vocabulary"):
            current_block = "vocabulary"
        elif line.startswith("theory"):
            current_block = "theory"
            theory_c = get_theory_name(line)
            theory_blocks[theory_c] = line + "\n"
            continue

        # Add the line to the corresponding block
        if current_block == "vocabulary":
            vocabulary_block += line + "\n"
        elif current_block == "theory":
            theory_blocks[theory_c] += line + "\n"

    return vocabulary_block.strip(), theory_blocks

def get_import_declarations(theory):
    """Extracts the names of theories that are imported in the theory"""

    pattern = r'import\s+(.*?)\.'
    import_blocks = list(set(re.findall(pattern, theory)))
    return import_blocks


def import_theory(target_theory, import_theory_name, import_theory_body):
    """Extends a given theory with the body of another theory and removes import
    declarations for the theory name"""
    
    pattern = fr'import\s+{import_theory_name}\.'
    remove_imports_theory = re.sub(pattern, '', target_theory)
    return extend_theory(remove_imports_theory, get_theory_body(import_theory_body))

def get_theory_body(theory):
    """Return the lines of the body of a given theory"""
    
    # Find the first occurrence of "}"
    first_bracket_index = theory.find('{') + 1

    # Find the last occurrence of "}"
    last_bracket_index = theory.rfind('}')

    # return lines of the substring
    return theory[first_bracket_index:last_bracket_index].split('\n')

def extend_vocabulary(vocabulary, new_lines):
    """Takes a vocabulary and a list of new lines (declarations)
    and appends the list of new lines to the end of the vocabulary.
    There is no check of syntax of new lines!"""   

    lines = vocabulary.split('\n')
    end = len(lines) - 1 
    lines[end:end] = new_lines
    return '\n'.join(lines)

def extend_theory(theory, new_lines):
    """Takes a theory and a list of new lines (formulae)
    and appends the list of new lines to the begining of the theory.
    There is no check of syntax of new lines!"""   

    lines = theory.split('\n')
    lines[1:1] = new_lines
    return '\n'.join(lines)

def new_theory(theory_name, vocabulary_name, body):
    """Create a theory block"""
    return "theory " + theory_name + ":" + vocabulary_name + "{" + body + "}"

def new_structure(structure_name, vocabulary_name, body):
    """Create a structure block"""
    return "structure " + structure_name + ":" + vocabulary_name + "{" + body + "}"

def merge(vocabulary, theories, structure = ""):
    """Merge vocabulary, list of theories, and strucutre together"""
    return vocabulary + "\n\n" + "\n\n".join(t for t in theories) + structure

def get_vocabulary_name(vocabulary):
    """Extracts the name of a vocabulary"""

    # Regex patern
    pattern = r'vocabulary\s+([A-Za-z0-9]+)\s*\{'
    match = re.search(pattern, vocabulary)

    # Check the patern
    if match:
        nema = match.group(1)
        return(nema)
    else:
        typer.secho(
            f'Error trying to get vocabulary name!',
            fg=typer.colors.RED,
        )
        sys.exit()

def get_theory_name(theory):
    """Extracts the name of a theory"""

    # Regex patern
    pattern = r'theory\s+([A-Za-z0-9]+)\s*:'
    match = re.search(pattern, theory)

    # Check the patern
    if match:
        nema = match.group(1)
        return(nema)
    else:
        typer.secho(
            f'Error trying to get theory name!',
            fg=typer.colors.RED,
        )
        sys.exit()
    
def find_K_atoms(theory):
    """Function searching for all K atoms in a theory"""

    # Regex patern
    pattern = r'K\[(.*?)\]\[(.*?)\]'
    k_blocks = list(set(re.findall(pattern, theory)))
    return k_blocks

def formula_free_vars(vocabulary, phi):
    """Find all the free variables in a formula phi and their typoes"""

    # Get the vocabulary name
    voc_name = get_vocabulary_name(vocabulary)

    # Storage variable
    free_vars = list()
    
    # Create a theory of the formula
    code = merge(vocabulary, [new_theory("T", voc_name, phi + ".")])

    # As long as there are parsing errors
    while True:
        try:
            # Try to parse theory containing only phi
            IDP.from_str(code)
            break
        except utils.IDPZ3Error as er:
            # When parsing fails extract missing symbol from the error
            match = re.search(r'Symbol not in vocabulary: (\w)', str(er))

            # If error is matching
            if match:
                # Get the variable name
                var = match.group(1)
                # Extend phi with quantification for the missing variable
                phi = "! " + var + ":" + phi
                # Create new theory with new phi
                code = merge(vocabulary, [new_theory("T", voc_name, phi + ".")])
                # Store free variable
                atom = find_atom_variable_occures_in(phi, var)
                var_type = get_argument_types_of_predicate(vocabulary, atom[0], atom[1])
                free_vars.append((var, var_type))
            else:
                # If there is another error
                typer.secho(
                    f'Error parsing free variables!',
                    fg=typer.colors.RED,
                )
                sys.exit()

    return free_vars

def find_atom_variable_occures_in(phi, var):
    """Find predicate symbol of an atom where variable occuress"""

    # Extract all atoms from the formula
    atom_pattern = r'([A-Za-z_]\w*)\(([^()]*)\)'
    atoms = re.findall(atom_pattern, phi)

    # Iterate over each atom to find var and its argument position
    for atom in atoms:
        atom_name = atom[0]
        arguments = atom[1].split(',')
        if var in arguments:
            argument_position = arguments.index(var)
            return(atom_name, argument_position)
    
    # TODO update equality atom search!!!
    # Extract all = atoms from the formula
    atom_pattern = r'([A-Za-z_]\w*)\(([^()]*)\)\s*=\s*([A-Za-z_]\w*)'
    atoms = re.findall(atom_pattern, phi)

    # Iterate over each = atom to find var and its argument position
    for atom in atoms:
        atom_name = atom[0]
        argument = atom[2]
        
        if var == argument:
            return(atom_name, -1)


    # If predicate was not found terminate with the error
    typer.secho(
        f'Error extracting predicate from formula "{phi}" for variable "{var}"!',
        fg=typer.colors.RED,
    )
    sys.exit()

def get_argument_types_of_predicate(vocabulary, predicate, position):
    """Extrtact the types of arguments of a predicate from a vocabulary"""
    
    pattern = fr'{predicate}\s*:\s*\(?\s*([A-Za-z_\*\s]*)\s*\)?\s*->\s*([A-Za-z_]+)'
    match = re.search(pattern, vocabulary)

    if match and position >= 0:
        elements_str = match.group(1)
        types = [element.strip() for element in elements_str.split('*')]
        return types[position]
    elif match:
        elements_str = match.group(2)
        return elements_str
    else:
        typer.secho(
            f'Error extracting argument type from predicate "{predicate}" possition "{position}"!',
            fg=typer.colors.RED,
        )
        sys.exit()

def entailment(vocabulary, theory, phi):
    """Use IDP-Z3 to check if phi is entailed by the"""

    # Get theory and voc names
    theory_name = get_theory_name(theory)
    voc_name = get_vocabulary_name(vocabulary)

    # Insert negation of phi to the theory
    new_theory = extend_theory(theory, ["~("+phi+")."])

    # Create a new code; i.e., merge voc + theory + empty structure
    code = merge(vocabulary, [new_theory], new_structure("S", voc_name, ""))

    # Create IDP-Z3 theory
    kb = IDP.from_str(code)
    T, S = kb.get_blocks(theory_name + ", S")
    idp_theory = Theory(T, S)

    # Use IDP-Z3 to generate models and check if there are any
    for model in idp_theory.expand():
        if model == "No models.":
            return True
        else:
            return False 

def propagate_for_predicate(vocabulary, theory, predicate_name):
    """Use IDP-Z3 to get theory propagations of theory the"""

    # Get the theory name
    theory_name = get_theory_name(theory)
    
    # Merge vocabulary and theory 
    code = merge(vocabulary, [theory])

    # Create IDP-Z3 theory 
    kb = IDP.from_str(code)
    T, S = kb.get_blocks(theory_name + ", S")
    idp_theory = Theory(T)

    # Use propagation inference
    prop_theory = idp_theory.propagate()
 
    # Find only relevant lines for the predicate
    relevant_propagations = []
    for line in str(prop_theory.assignments).splitlines():
        if predicate_name in line:
            relevant_propagations.append(line)

    # If the predicate has some interpretation
    if relevant_propagations:
        # Return propagations
        return '\n'.join(relevant_propagations)
    else:
        # Return empty interpretation
        return predicate_name + " := {}."



            
